<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 3/14/2022
 * Time: 8:49 AM
 */
?>
<nav class="vertical-navigation" aria-label="Vertical Navigation">
    <div class="vertical-navigation-header">
        <i class="axtronic-icon-bars"></i>
        <span class="vertical-navigation-title">Shop by Categories</span>
    </div>
    <div class="vertical-menu">
        <ul class="nav menu">
            <li class="nav-item has-mega-menu">
                <a class="nav-link" href="#">
                    <i class="axtronic-icon-monitor-mobbile"></i> Computers & Accessories
                </a>
                <ul class="nav sub-menu mega-menu">
                    <li class="mega-menu-item">
                        <div class="row ">
                            <div class="col-sm-3">
                                <ul class="nav list-items">
                                    <li class="list-item">
                                        <a href="#">
                                            <span class="list-text">Gaming PC</span>
                                        </a>
                                    </li>
                                    <li class="list-item">
                                        <a href="#">
                                            <span class="list-text">Office PC</span>
                                        </a>
                                    </li>
                                    <li class="list-item">
                                        <a href="#">
                                            <span class="list-text">Laptops</span>
                                        </a>
                                    </li>
                                    <li class="list-item">
                                        <a href="#">
                                            <span class="list-text">Screen</span>
                                        </a>
                                    </li>
                                    <li class="list-item">
                                        <a href="#">
                                            <span class="list-text">Keyboard</span>
                                        </a>
                                    </li>
                                    <li class="list-item">
                                        <a href="#">
                                            <span class="list-text">Gaming Chair</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-5 border-start">
                                <ul class="nav list-items list-product-items">
                                    <li class="list-item product-item">
                                        <div class="product-list-wrap">
                                            <div class="product-transition">
                                                <div class="product-img-wrap ">
                                                    <div class="product-image"><img src="{{ theme_url('Axtronic/images/iPhone201320.jpg') }}" alt="Axtronic WooCommerce" ></div>
                                                </div>
                                            </div>
                                            <div class="product-caption">
                                                <h2 class="product__title"><a href="#">iPhone 13 Pro Max 256GB</a></h2>
                                                <div class="star-rating" role="img" title="70%">
                                                    <div class="back-stars">
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>

                                                        <div class="front-stars" style="width: 70%">
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="price">
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            110.00
                                                        </bdi>
                                                    </span>
                                                    –
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            150.00
                                                        </bdi>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-item product-item">
                                        <div class="product-list-wrap">
                                            <div class="product-transition">
                                                <div class="product-img-wrap ">
                                                    <div class="product-image"><img src="{{ theme_url('Axtronic/images/iPhone201320.jpg') }}" alt="Axtronic WooCommerce" ></div>
                                                </div>
                                            </div>
                                            <div class="product-caption">
                                                <h2 class="product__title"><a href="#">iPhone 13 Pro Max 256GB</a></h2>
                                                <div class="star-rating" role="img" title="70%">
                                                    <div class="back-stars">
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>

                                                        <div class="front-stars" style="width: 70%">
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="price">
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            110.00
                                                        </bdi>
                                                    </span>
                                                    –
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            150.00
                                                        </bdi>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-item product-item">
                                        <div class="product-list-wrap">
                                            <div class="product-transition">
                                                <div class="product-img-wrap ">
                                                    <div class="product-image"><img src="{{ theme_url('Axtronic/images/iPhone201320.jpg') }}" alt="Axtronic WooCommerce" ></div>
                                                </div>
                                            </div>
                                            <div class="product-caption">
                                                <h2 class="product__title"><a href="#">iPhone 13 Pro Max 256GB</a></h2>
                                                <div class="star-rating" role="img" title="70%">
                                                    <div class="back-stars">
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>

                                                        <div class="front-stars" style="width: 70%">
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="price">
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            110.00
                                                        </bdi>
                                                    </span>
                                                    –
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            150.00
                                                        </bdi>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-item product-item">
                                        <div class="product-list-wrap">
                                            <div class="product-transition">
                                                <div class="product-img-wrap ">
                                                    <div class="product-image"><img src="{{ theme_url('Axtronic/images/iPhone201320.jpg') }}" alt="Axtronic WooCommerce" ></div>
                                                </div>
                                            </div>
                                            <div class="product-caption">
                                                <h2 class="product__title"><a href="#">iPhone 13 Pro Max 256GB</a></h2>
                                                <div class="star-rating" role="img" title="70%">
                                                    <div class="back-stars">
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>
                                                        <i class="axtronic-icon-star" aria-hidden="true"></i>

                                                        <div class="front-stars" style="width: 70%">
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                            <i class="axtronic-icon-star-sharp" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="price">
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            110.00
                                                        </bdi>
                                                    </span>
                                                    –
                                                    <span class="woocommerce-Price-amount amount">
                                                        <bdi>
                                                            <span class="woocommerce-Price-currencySymbol">$</span>
                                                            150.00
                                                        </bdi>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <img src="{{ theme_url('Axtronic/images/megamenu-img1.jpg') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-mobile"></i>Cell Phones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-airpods"></i> Headphones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-house-2"></i> Home Audio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-glass"></i> Wearable Technology</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-watch"></i> Watchs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-electricity"></i> GPS & Navigation</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-gameboy"></i> Game Consoles & Accessories</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> <i class="axtronic-icon-discount-shape"></i>Special Offer</a>
            </li>
        </ul>
    </div>
</nav>
