<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 3/25/2022
 * Time: 4:16 PM
 */
?>

<div class="axtronic-banner">
    <div class="banner-wrap ">
        <div class="row align-items-center">
            <div class="col-sm-4">
                <div class="item-content d-flex align-content-center align-items-start flex-column justify-content-center">
                    <div class="sub-title"><span>20% OFF</span>  14 Dec to 16 Dec</div>
                    <h2>Beats Solo3 Wireless</h2>
                    <div class="price">$320 <span>$400</span></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="item-image">
                    <img src="{{ theme_url('Axtronic/images/banner-section-beat.png') }}" alt="">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="item-content d-flex align-content-start align-items-start flex-column justify-content-end">
                    <span class="sub-title"><span>Special Offer</span></span>
                    <h3>Christmas Sale</h3>
                    <p>
                        Hurry up. Limited period offer. <br>
                        Best chance to bring Beats Solo3<br>
                        to your home.
                    </p>
                    <a href="#" class="button item-button">Shop Now <i class="axtronic-icon-angle-right"></i> </a>
                </div>
            </div>
        </div>
    </div>
</div>
