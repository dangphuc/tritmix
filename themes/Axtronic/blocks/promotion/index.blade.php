{{--Axtronic promotions--}}
<div class="axtronic-promotions pb-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="promotions-item">
                    <div class="item-bg" style="background-image: url({{ theme_url('Axtronic/images/banner1.jpg') }});"></div>
                    <div class="item-content d-flex align-content-start align-items-start flex-column  justify-content-end">
                        <span class="sub-title">Sound</span>
                        <h3>Headphone</h3>
                        <p>
                            <span>Start from</span>
                            <br>$699.00
                        </p>
                        <a href="#" class="item-button">Shop Now <i class="axtronic-icon-angle-right"></i> </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="promotions-item">
                    <div class="item-bg" style="background-image: url({{ theme_url('Axtronic/images/banner2.jpg') }});"></div>
                    <div class="item-content d-flex align-content-start align-items-start flex-column justify-content-end">
                        <span class="sub-title">Sound</span>
                        <h3>Headphone</h3>
                        <p>
                            <span>Start from</span>
                            <br>$699.00
                        </p>
                        <a href="#" class="item-button">Shop Now <i class="axtronic-icon-angle-right"></i> </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="promotions-item">
                    <div class="item-bg" style="background-image: url({{ theme_url('Axtronic/images/banner3.jpg') }});"></div>
                    <div class="item-content d-flex align-content-start align-items-start flex-column justify-content-end">
                        <span class="sub-title">Sound</span>
                        <h3>Headphone</h3>
                        <p>
                            <span>Start from</span>
                            <br>$699.00
                        </p>
                        <a href="#" class="item-button">Shop Now <i class="axtronic-icon-angle-right"></i> </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="promotions-item">
                    <div class="item-bg" style="background-image: url({{ theme_url('Axtronic/images/banner4.jpg') }});"></div>
                    <div class="item-content d-flex align-content-start align-items-start flex-column justify-content-start">
                        <span class="sub-title">Sound</span>
                        <h3>Headphone</h3>
                        <p>
                            <span>Start from</span>
                            <br>$699.00
                        </p>
                        <a href="#" class="item-button">Shop Now <i class="axtronic-icon-angle-right"></i> </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="promotions-item">
                    <div class="item-bg" style="background-image: url({{ theme_url('Axtronic/images/banner5.jpg') }});"></div>
                    <div class="item-content d-flex align-content-start align-items-start flex-column justify-content-start">
                        <span class="sub-title">Sound</span>
                        <h3>Headphone</h3>
                        <p>
                            <span>Start from</span>
                            <br>$699.00
                        </p>
                        <a href="#" class="item-button">Shop Now <i class="axtronic-icon-angle-right"></i> </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="promotions-item">
                    <div class="item-bg" style="background-image: url({{ theme_url('Axtronic/images/banner6.jpg') }});"></div>
                    <div class="item-content d-flex align-content-start align-items-start flex-column justify-content-start">
                        <span class="sub-title">Sound</span>
                        <h3>Headphone</h3>
                        <p>
                            <span>Start from</span>
                            <br>$699.00
                        </p>
                        <a href="#" class="item-button">Shop Now <i class="axtronic-icon-angle-right"></i> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{--Axtronic Banner--}}

<div class="container">
    @include("blocks.banner.index")
</div>


{{--Axtronic Testimonial--}}

@include("blocks.testimonial.index")


{{--Axtronic News--}}

@include("blocks.news.index")


{{--Axtronic Brands--}}

@include("blocks.brand.index")
