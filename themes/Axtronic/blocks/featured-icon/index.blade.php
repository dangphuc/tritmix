<div class="axtronic-site-features pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-sm-6 mb-xl-0 mb-4">
                <div class="d-flex align-items-center justify-content-center box">
                    <div class="item-icon">
                        <i class="axtronic-icon-group"></i>
                    </div>
                    <div class="ms-3">
                        <h4 >Free Shipping</h4>
                        <p class="mb-0">Free shipping on all order</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6 mb-xl-0 mb-4">
                <div class="d-flex align-items-center justify-content-center box">
                    <div class="item-icon">
                        <i class="axtronic-icon-medal-star"></i>
                    </div>
                    <div class="ms-3">
                        <h4 >Free Shipping</h4>
                        <p class="mb-0 ">Free shipping on all order</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6 mb-xl-0 mb-4">
                <div class="d-flex align-items-center justify-content-center box">
                        <div class="item-icon">
                            <i class="axtronic-icon-heart-circle"></i>
                        </div>
                    <div class=" ms-3">
                        <h4 >Free Shipping</h4>
                        <p class="mb-0">Free shipping on all order</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6 mb-xl-0 mb-4">
                <div class="d-flex align-items-center justify-content-center box">
                    <div class="item-icon">
                        <i class="axtronic-icon-wallet"></i>
                    </div>
                    <div class=" ms-3">
                        <h4>Free Shipping</h4>
                        <p class="mb-0 ">Free shipping on all order</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--Axtronic Category--}}

<div class="axtronic-category">
    <div class="container">
        <h2 class="heading-title">Category</h2>
        <div class="swiper-slider-icon swiper-container ">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
                <div class="swiper-slide">
                    <div class="item-icons">
                        <a href="#">
                            <i class="axtronic-icon-monitor-mobbile"></i>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="#">Laptop, PC, Monitor </a></h3>
                </div>
            </div>
            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>

    </div>

</div>
