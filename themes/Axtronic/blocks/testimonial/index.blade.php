<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 3/25/2022
 * Time: 4:18 PM
 */
?>
<div class="axtronic-testimonial">
    <div class="container">
        <h2 class="heading-title text-center">Feedback from Customers</h2>
        <div class="swiper-slider-testimonial swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="inner">
                        <div class="testimonial-image">
                            <img src="{{ theme_url('Axtronic/images/avarta1.png') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                        </div>
                        <div class="caption">
                            <div class="details">
                                <h2 class="name">John Dee</h2>
                                <h3 class="job">Digital Marketer</h3>
                            </div>
                            <div class="stars">
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                            </div>
                        </div>
                        <p class="testimonial-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna lirabe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="inner">
                        <div class="testimonial-image">
                            <img src="{{ theme_url('Axtronic/images/avarta2.png') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                        </div>
                        <div class="caption">
                            <div class="details">
                                <h2 class="name">John Dee</h2>
                                <h3 class="job">Digital Marketer</h3>
                            </div>
                            <div class="stars">
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                            </div>
                        </div>
                        <p class="testimonial-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna lirabe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                        </p>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="inner">
                        <div class="testimonial-image">
                            <img src="{{ theme_url('Axtronic/images/avarta3.png') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                        </div>
                        <div class="caption">
                            <div class="details">
                                <h2 class="name">John Dee</h2>
                                <h3 class="job">Digital Marketer</h3>
                            </div>
                            <div class="stars">
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                            </div>
                        </div>
                        <p class="testimonial-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna lirabe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                        </p>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="inner">
                        <div class="testimonial-image">
                            <img src="{{ theme_url('Axtronic/images/avarta2.png') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                        </div>
                        <div class="caption">
                            <div class="details">
                                <h2 class="name">John Dee</h2>
                                <h3 class="job">Digital Marketer</h3>
                            </div>
                            <div class="stars">
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                            </div>
                        </div>
                        <p class="testimonial-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna lirabe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                        </p>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="inner">
                        <div class="testimonial-image">
                            <img src="{{ theme_url('Axtronic/images/avarta3.png') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                        </div>
                        <div class="caption">
                            <div class="details">
                                <h2 class="name">John Dee</h2>
                                <h3 class="job">Digital Marketer</h3>
                            </div>
                            <div class="stars">
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                            </div>
                        </div>
                        <p class="testimonial-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna lirabe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                        </p>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="inner">
                        <div class="testimonial-image">
                            <img src="{{ theme_url('Axtronic/images/avarta2.png') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                        </div>
                        <div class="caption">
                            <div class="details">
                                <h2 class="name">John Dee</h2>
                                <h3 class="job">Digital Marketer</h3>
                            </div>
                            <div class="stars">
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                            </div>
                        </div>
                        <p class="testimonial-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna lirabe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                        </p>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="inner">
                        <div class="testimonial-image">
                            <img src="{{ theme_url('Axtronic/images/avarta3.png') }}" class="attachment-full size-full" alt="Axtronic WooCommerce" >
                        </div>
                        <div class="caption">
                            <div class="details">
                                <h2 class="name">John Dee</h2>
                                <h3 class="job">Digital Marketer</h3>
                            </div>
                            <div class="stars">
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                                <i class="axtronic-icon-star-sharp"></i>
                            </div>
                        </div>
                        <p class="testimonial-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna lirabe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                        </p>

                    </div>
                </div>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
