<!-- Slider main container -->
<div class="container">

    <div class="banner-slider swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper axtronic-modern-slider ">
            <!-- Slides -->
            <div class="swiper-slide">
                <div class="axtronic-box">
                    <div class="axtronic-box-inner">
                        <div class="slide-bg-wrap axtronic-image">
                            <div class="slide-bg image" style="background-image: url('{{ theme_url('Axtronic/images/mainslider-1.jpg') }}')"></div>
                        </div>
                        <div class="slide-content">
                            <div class="slide-layers">
                                <div class="title-wrap-line">
                                    <div class="sub-title-wrap">
                                        <h4 class="sub-title">On Sale</h4>
                                    </div>
                                    <div class="title-wrap">
                                        <h3 class="title">New arrivals Smartphone
                                        </h3>
                                    </div>
                                </div>
                                <div class="description-wrap">
                                    <div class="description">
                                        <p><span >original price $799.00</span><br>$550.99</p>
                                    </div>
                                </div>
                                <div class="button-wrap">
                                    <a class="elementor-button" href="#">
                                        <span class="button-text">Shop Now</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="axtronic-box">
                    <div class="axtronic-box-inner">
                        <div class="slide-bg-wrap axtronic-image">
                            <div class="slide-bg image" style="background-image: url('{{ theme_url('Axtronic/images/mainslider-2.jpg') }}')"></div>
                        </div>
                        <div class="slide-content">
                            <div class="slide-layers">
                                <div class="title-wrap-line">
                                    <div class="sub-title-wrap">
                                        <h4 class="sub-title">On Sale</h4>
                                    </div>
                                    <div class="title-wrap">
                                        <h3 class="title">New arrivals Smartphone
                                        </h3>
                                    </div>
                                </div>
                                <div class="description-wrap">
                                    <div class="description">
                                        <p><span>original price $799.00</span><br>$550.99</p>
                                    </div>
                                </div>
                                <div class="button-wrap">
                                    <a class="elementor-button" href="#">
                                        <span class="button-text">Shop Now</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="axtronic-box">
                    <div class="axtronic-box-inner">
                        <div class="slide-bg-wrap axtronic-image">
                            <div class="slide-bg image" style="background-image: url('{{ theme_url('Axtronic/images/mainslider-3.jpg') }}')"></div>
                        </div>
                        <div class="slide-content">
                            <div class="slide-layers">
                                <div class="title-wrap-line">
                                    <div class="sub-title-wrap">
                                        <h4 class="sub-title">On Sale</h4>
                                    </div>
                                    <div class="title-wrap">
                                        <h3 class="title">New arrivals Smartphone
                                        </h3>
                                    </div>
                                </div>
                                <div class="description-wrap">
                                    <div class="description">
                                        <p><span>original price $799.00</span><br>$550.99</p>
                                    </div>
                                </div>
                                <div class="button-wrap">
                                    <a class="elementor-button" href="#">
                                        <span class="button-text">Shop Now</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

    </div>
</div>

